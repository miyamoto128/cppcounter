#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <string>
#include <vector>

#include "../src/counter.hpp"

using namespace std;
using namespace collection;

class CounterTest : public CppUnit::TestFixture {
    private:
        CPPUNIT_TEST_SUITE(CounterTest);
        CPPUNIT_TEST(testSize);
        CPPUNIT_TEST(testIncrement);
        CPPUNIT_TEST(testMerge);
        CPPUNIT_TEST(testDifference);
        CPPUNIT_TEST(testMostCommon);
        CPPUNIT_TEST_SUITE_END();

        Counter<int> counter_integers_;
        Counter<string> counter_strings_;

    public:
        void setUp() {
            counter_integers_ = { 10, 20, 30 };
            counter_strings_ = { "red", "green", "blue" };
        }

        void tearDown() {}


        void testSize() {
            Counter<float> empty;
            CPPUNIT_ASSERT(empty.size() == 0);
            CPPUNIT_ASSERT(counter_integers_.size() == 3);
            CPPUNIT_ASSERT(counter_strings_.size() == 3);
        }

        void testIncrement() {
            CPPUNIT_ASSERT(counter_integers_.size() == 3);

            counter_integers_[10]++;
            ++counter_integers_[10];
            counter_integers_[10] += 9;
            counter_integers_[10] -= 5;
            CPPUNIT_ASSERT(counter_integers_[10] == 7);


            counter_integers_[20]--;
            --counter_integers_[20];
            counter_integers_[20] -= 1000;
            CPPUNIT_ASSERT(counter_integers_[20] == 0);

            counter_integers_[30]++;
            CPPUNIT_ASSERT(counter_integers_[30] == 2);

            counter_integers_[40]++;
            CPPUNIT_ASSERT(counter_integers_[40] == 1);

            
            CPPUNIT_ASSERT(counter_integers_[50] == 0);
            CPPUNIT_ASSERT(counter_integers_[100] == 0);
            CPPUNIT_ASSERT(counter_integers_[500] == 0);
        }

        void testMerge() {
            Counter<string> c1 = counter_strings_;
            CPPUNIT_ASSERT(c1.size() == 3);

            Counter<string> c2{"red", "green", "yellow"};
            c1 += c2;

            CPPUNIT_ASSERT(c1.size() == 4);
            CPPUNIT_ASSERT(c1["red"] == 2);
            CPPUNIT_ASSERT(c1["green"] == 2);
            CPPUNIT_ASSERT(c1["blue"] == 1);
            CPPUNIT_ASSERT(c1["yellow"] == 1);
        }

         void testDifference() {
            Counter<string> c1 = counter_strings_;
            CPPUNIT_ASSERT(c1.size() == 3);

            Counter<string> c2{"red"};
            c2["green"] += 5;
            c1 -= c2;

            CPPUNIT_ASSERT(c1.size() == 1);
            CPPUNIT_ASSERT(c1["red"] == 0);
            CPPUNIT_ASSERT(c1["green"] == 0);
            CPPUNIT_ASSERT(c1["blue"] == 1);
        }

        void testMostCommon() {
            counter_strings_["green"] = 10;
            counter_strings_["yellow"] += 5;
            counter_strings_["blue"]++;

            CPPUNIT_ASSERT(counter_strings_.size() == 4);
            CPPUNIT_ASSERT(counter_strings_["red"] == 1);
            CPPUNIT_ASSERT(counter_strings_["blue"] == 2);
            CPPUNIT_ASSERT(counter_strings_["yellow"] == 5);
            CPPUNIT_ASSERT(counter_strings_["green"] == 10);

            auto mostCommon = counter_strings_.most_common();
            CPPUNIT_ASSERT(mostCommon.size() == 4);

            mostCommon = counter_strings_.most_common(1);
            CPPUNIT_ASSERT(mostCommon.size() == 4);

            mostCommon = counter_strings_.most_common(2);
            CPPUNIT_ASSERT(mostCommon.size() == 3);
            CPPUNIT_ASSERT(mostCommon["blue"] == 2);
            CPPUNIT_ASSERT(mostCommon["yellow"] == 5);
            CPPUNIT_ASSERT(mostCommon["green"] == 10);

            mostCommon = counter_strings_.most_common(5);
            CPPUNIT_ASSERT(mostCommon.size() == 2);
            CPPUNIT_ASSERT(mostCommon["yellow"] == 5);
            CPPUNIT_ASSERT(mostCommon["green"] == 10);

            mostCommon = counter_strings_.most_common(10);
            CPPUNIT_ASSERT(mostCommon.size() == 1);
            CPPUNIT_ASSERT(mostCommon["green"] == 10);

            mostCommon = counter_strings_.most_common(11);
            CPPUNIT_ASSERT(mostCommon.size() == 0);
        }
};

CPPUNIT_TEST_SUITE_REGISTRATION(CounterTest);