#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

#include "counter.hpp"


std::vector<std::string> split_into_words(const std::string& text);

int main() {
    std::string text = "It's hardware that makes a machine fast. "
        "It's software that makes a fast machine slow. "
        "Craig Bruce";

    std::cout << std::endl << "Count words from:" << std::endl << "\"" << text << "\"";

    collection::Counter<std::string> counter(split_into_words(text));

    std::cout << std::endl << "-> ";
    std::for_each(counter.begin(), 
        counter.end(), 
        [](const auto & pair){ std::cout << "\"" << pair.first << "\": " << pair.second.value() << "  "; }
    );

    std::cout << std::endl << std::endl << "Most commons";

    const auto mostCommon = counter.most_common(2);

    std::cout << std::endl << "-> ";
    std::for_each(mostCommon.begin(), 
        mostCommon.end(), 
        [](const auto & pair){ std::cout << "\"" << pair.first << "\": " << pair.second.value() << "  "; }    
    );

    std::cout << std::endl << std::endl;

    return 0;
}


std::vector<std::string> split_into_words(const std::string& text) {
    std::istringstream iss(text);
    std::vector<std::string> words(
        (std::istream_iterator<std::string>(iss)), 
        std::istream_iterator<std::string>()
    );
    return words;
}