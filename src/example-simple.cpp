#include <iostream>

#include "counter.hpp"



int main() {
    collection::Counter counter({"koala", "panda", "firefox"});
    counter["koala"]++;
    counter["panda"]--;
    counter["firefox"] += 4;

    for (const auto& [element, count]: counter) {
        std::cout << "* \"" << element << "\" count: " << count.value() << std::endl;
    }
    
    return 0;
}
