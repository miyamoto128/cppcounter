
# Counter container

The Counter container works on top of a [std::unordered_map](https://en.cppreference.com/w/cpp/container/unordered_map) to ease the count of multiple elements. Inspired by the cool [collections.Counter](https://docs.python.org/3/library/collections.html#collections.Counter) in Python :)


# Usage

Simply pick and include the `counter.hpp` from the `src` in your project.

Here is a usage example:
```cpp
collection::Counter counter({"koala", "panda", "firefox"});
counter["koala"]++;
counter["panda"]--;
counter["firefox"] += 4;
 
for (const auto& [element, count]: counter) {
    std::cout << "* \"" << element << "\" count: " << count.value() << std::endl;
}
```

Retrieve this example and others within the `src/example-*.cpp` files.


# Documentation

Online [Doxygen](https://www.doxygen.nl/index.html) generated documentation is available:

https://miyamoto128.gitlab.io/cppcounter/html/classcollection_1_1_counter.html


# Build and try

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Execute examples
From `build` directory,
```
$ ./example-simple
```
and
```
$ ./example-words_counter
```

## Execute unit-tests
From `build` directory,
```
$ ./unit-tests
```
